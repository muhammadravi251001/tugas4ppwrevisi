"""tugas4ppwrevisi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from django.contrib import admin
from django.urls import path

from . import index

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('', index.webpage1, name='webpage1'),
    path('story1/', index.webpage2, name='webpage2'),
    path('story3/', index.webpage3, name='webpage3'),
    path('story3tambahan/', index.webpage4, name='webpage4'),



    #path('story1/', include('story1.urls')),
    #path('story3/', include('story3.urls')),
    #path('story3tambahan/', include('story3tambahan.urls')),
]
